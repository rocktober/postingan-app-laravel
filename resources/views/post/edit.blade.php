@extends('layouts.master')

@section('title')
Edit post
@endsection

@section('content')
<form action="/post/{{ $post -> id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Title</label>
        <input type="text" name="title" value={{ $post -> title }} class="form-control">
    </div>
    @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Content</label>
        <textarea name="content" class="form-control my-editor" cols="30" rows="10">{{ $post -> content }}</textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" name="thumbnail" class="form-control">
    </div>
    @error('thumbnail')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Category</label>
        <select name="category" class="form-control">
            <option value="">--choose category--</option>
            @forelse ($category as $item)
            @if ($item -> id === $post -> category_id)
            <option value="{{ $item -> id }}" selected>{{ $item -> name }}</option>
            @else
            <option value="{{ $item -> id }}">{{ $item -> name }}</option>
            @endif
            @empty
            <option value="">Tidak Ada Data Category</option>
            @endforelse
        </select>
    </div>
    @error('category')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection