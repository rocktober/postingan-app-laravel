@extends('layouts.master')

@section('title')
Create post
@endsection

@section('content')
<form action="/post" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Title</label>
        <input type="text" name="title" class="form-control">
    </div>
    @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Content Post</label>
        <textarea name="content" class="my-editor form-control" id="content" cols="30" rows="10"></textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" name="thumbnail" class="form-control">
    </div>
    @error('thumbnail')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Category</label>
        <select name="category" class="form-control">
            <option value="">--choose category--</option>
            @forelse ($category as $item)
            <option value={{ $item -> id }}>{{ $item -> name }}</option>
            @empty
            <option value="">Tidak Ada Data Category</option>
            @endforelse
        </select>
    </div>
    @error('category')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Create</button>
</form>
@endsection