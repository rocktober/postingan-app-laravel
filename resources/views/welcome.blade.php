@extends('layouts.master')

@section('title')
Dashboard
@endsection

@section('content')

<div class="info">
    <div class="image">
        <img class="float-left img-fluid" width="400px"
            src="{{asset('image/reshot-illustration-couple-solving-puzzle-QXBW4S3U7J.png')  }}" alt="">
    </div>
    <div class="welcome text-center">
        @auth
        <h1>Welcome, {{ Auth::user()->name }}</h1>
        @endauth
        @guest
        <h1>Welcome</h1>
        @endguest
    </div>
</div>

@endsection