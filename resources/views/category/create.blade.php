@extends('layouts.master')

@section('title')
Create category
@endsection

@section('content')
<form action="/category" method="POST">
    @csrf
    <div class="form-group">
        <label>Name Category</label>
        <input type="text" name="name" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Description Category</label>
        <textarea name="description" class="form-control my-editor" cols="30" rows="10"></textarea>
    </div>
    @error('description')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Create</button>
</form>
@endsection