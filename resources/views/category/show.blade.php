@extends('layouts.master')

@section('title')
List category
@endsection

@section('content')
<a href="/category/create" class="btn btn-primary btn-sm">Create</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($category as $key => $value)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $value -> name }}</td>
            <td>
                <form action="/category/{{ $value -> id }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/category/{{ $value -> id }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/category/{{ $value -> id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td>Tidak Ada Data</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection