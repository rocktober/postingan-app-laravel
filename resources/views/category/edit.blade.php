@extends('layouts.master')

@section('title')
Edit category
@endsection

@section('content')
<form action="/category/{{$category -> id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Name Category</label>
        <input type="text" name="name" value={{ $category -> name }} class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Description Category</label>
        <textarea name="description" class="form-control my-editor" cols="30"
            rows="10">{{ $category -> description }}</textarea>
    </div>
    @error('description')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection