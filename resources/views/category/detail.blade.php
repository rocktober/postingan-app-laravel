@extends('layouts.master')

@section('title')
Detail category
@endsection

@section('content')
<h1>{{ $category -> name }}</h1>
<p>{{ $category -> description }}</p>

<div class="row">
    @forelse ($category->post as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('image/'. $item -> thumbnail)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h3>{{ $item -> title }}</h3>
                <span class="badge badge-success mb-2">{{ $item->category->name }}</span>
                <p class="card-text">{{ Str::limit($item -> content, 100) }}</p>
                <a href="/post/{{ $item -> id }}" class="btn btn-secondary btn-sm btn-block">Read More</a>
            </div>
        </div>
    </div>
    @empty
    <h1>No post in this category</h1>
    @endforelse
</div>
<a href="/category" class="btn btn-secondary btn-sm">Back</a>
@endsection