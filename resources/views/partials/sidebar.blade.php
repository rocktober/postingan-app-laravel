<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{asset('template/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                alt="User Image">
        </div>
        <div class="info">
            @auth
            <a href="/profile" class="d-block">{{ Auth::user()->name }}</a>
            @endauth
            @guest
            <a href="/login" class="d-block">Guest</a>
            @endguest
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
   with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon bi bi-speedometer2"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon bi bi-card-list"></i>
                    <p>
                        Halaman
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/table" class="nav-link">
                            <i class="bi bi-table nav-icon"></i>
                            <p>Table</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/data-tables" class="nav-link">
                            <i class="bi bi-table nav-icon"></i>
                            <p>Data Table</p>
                        </a>
                    </li>
                </ul>
            </li>
            @auth
            <li class="nav-item">
                <a href="/category" class="nav-link">
                    <i class="nav-icon bi bi-bookmark"></i>
                    <p>
                        Category
                    </p>
                </a>
            </li>
            @endauth
            <li class="nav-item">
                <a href="/post" class="nav-link">
                    <i class="nav-icon bi bi-card-text"></i>
                    <p>
                        Post
                    </p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>