@extends('layouts.master')

@section('title')
Profile
@endsection

@section('content')
<form action="/profile/{{ $profile->id }}" class="text-center">
    <div class="form-group">
        <label>Email</label>
        <p>{{ $profile->user->email }}</p>
    </div>
    <div class="form-group">
        <label>Name</label>
        <p name='name'>{{ $profile->user->name }}</p>
    </div>
    <div class="form-group">
        <label>Age</label>
        <p name='age'>{{ $profile->age }}</p>
    </div>
    <div class="form-group">
        <label>Phone</label>
        <p name='phone'>{{ $profile->phone }}</p>
    </div>

    <div class="col">
        <a href="/profile/{{ $profile -> id }}/edit" class="btn btn-info btn-sm mt-4">Edit</a>
    </div>
</form>
@endsection