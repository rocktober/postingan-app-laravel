@extends('layouts.master')

@section('title')
Edit Profile
@endsection

@section('content')
<form action="/profile/{{ $profile->id }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="name" value={{ $profile->user->name }} class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Age</label>
        <input type="text" name="age" value-{{ $profile->age }} class="form-control">
    </div>
    @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Phone</label>
        <input type="text" name="phone" class="form-control">
    </div>
    @error('phone')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection