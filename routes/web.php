<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TagController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/table', function () {
    return view('table.table');
});

Route::get('/data-tables', function () {
    return view('table.data-table');
});

Route::group(['middleware' => ['auth']], function () {
    // CRUD Categories
    Route::get('/category/create', [CategoryController::class, 'create']);
    Route::post('/category', [CategoryController::class, 'store']);
    Route::get('/category', [CategoryController::class, 'index']);
    Route::get('/category/{category_id}', [CategoryController::class, 'show']);
    Route::get('/category/{category_id}/edit', [CategoryController::class, 'edit']);
    Route::put('/category/{category_id}', [CategoryController::class, 'update']);
    Route::delete('/category/{category_id}', [CategoryController::class, 'destroy']);
    // CRUD Tags
    Route::post('/tag', [TagController::class, 'tag']);
});
// CRUD Posts
Route::resource('post', PostController::class);
// CRUD Profile
Route::resource('profile', ProfileController::class)->only(['index', 'edit', 'update']);

Auth::routes();
