<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Category;
use RealRashid\SweetAlert\Facades\Alert;

class CategoryController extends Controller
{
    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        DB::table('categories')->insert([
            'name' => $request['name'],
            'description' => $request['description'],
        ]);

        Alert::success('Success', 'Success Add New Category');

        return redirect('/category');
    }

    public function index()
    {
        $category = DB::table('categories')->get();
        return view('category.show', compact('category'));
    }

    public function show($id)
    {
        $category = Category::find($id);
        return view('category.detail', compact('category'));
    }

    public function edit($id)
    {
        $category = DB::table('categories')->where('id', $id)->first();
        return view('category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        DB::table('categories')
            ->where('id', $id)
            ->update(
                [
                    'name' => $request['name'],
                    'description' => $request['description']
                ]
            );

        Alert::success('Success', 'Success Update Category');

        return redirect('/category');
    }

    public function destroy($id)
    {
        DB::table('categories')->where('id', $id)->delete();

        Alert::warning('Category Has Deleted');

        return redirect('/category');
    }
}
