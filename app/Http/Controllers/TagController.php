<?php

namespace App\Http\Controllers;

use App\Models\Tag;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    public function tag(Request $request)
    {
        $tag = new Tag;

        $tag->name = $request->name;
        $tag->user_id = Auth::id();
        $tag->post_id = $request->post_id;

        $tag->save();

        return redirect('/post/' . $request->post_id);
    }
}
